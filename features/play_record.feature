Feature: Play Record feature

  Scenario: As user i should be able to listen a record
    Given I wait for 2 seconds
    Given I press "Add Podcast"
    And I wait for 1 seconds
    And I press the "BROWSE GPODDER.NET" button
    And I press "Linux Outlaws"
    And I press the "SUBSCRIBE" button
    And I press the "OPEN PODCAST" button
    And I wait for 1 seconds
    Given I press "Important Message"
    And I wait for 1 seconds
    And I touch the "Stream" text
    Then I should see "1.00X"