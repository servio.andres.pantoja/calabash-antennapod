Feature: Subscribe feature

  Scenario: As user i should be able to subscribe to channel
    Given I wait for 2 seconds
    Given I press "Add Podcast"
    And I wait for 1 seconds
    And I press the "BROWSE GPODDER.NET" button
    And I press "Linux Outlaws"
    And I press the "SUBSCRIBE" button
    And I press the "OPEN PODCAST" button
    Then I should see "Linux Outlaws"